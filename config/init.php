<?php
session_start();
// config file
require_once 'config.php';
// helper file
require_once 'helpers/system_helper.php';
// Autoload our classes
function __autoload($className)
{
    require_once 'lib/'.$className.'.php';
}

?>