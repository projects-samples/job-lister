<?php include 'inc/header.php'; ?>

    <h2 class="page-header"><?php echo $job->job_title; ?> (<?php echo $job->location; ?>)</h2>
    <small>Posted by <?php echo $job->contact_user; ?> on <?php echo $job->post_date; ?></small>
    <hr>
    <p class="lead"><?php echo $job->description; ?></p>
    <ul class="list-group">
        <li class="list-group-item"><strong>Company: </strong><?php echo $job->company; ?></li>
        <li class="list-group-item"><strong>Salary: </strong><?php echo $job->salary; ?></li>
        <li class="list-group-item"><strong>Contact Email: </strong><?php echo $job->contact_email; ?></li>
    </ul>
    <br>
    <br>
    <div class="controls">
        <a href="index.php" class="btn go-back-btn">Go back</a>
        <a href="edit.php?id=<?php echo $job->id; ?>" class="btn edit-btn">Edit</a>
        <form action="job.php" method="post" id="delete-form">
            <input type="hidden" name="del_id" value="<?php echo $job->id; ?>">
            <input type="submit" value="Delete" class="btn delete-btn" name="submit">
        </form>
    </div>

    <div class="comment-section">
        <form action="job.php" method="post" id="comment-form">
            <input type="hidden" name="post_id" value="<?php echo $job->id; ?>">
            <textarea name="content"></textarea>
            <input type="submit" value="Comment" name="comment" class="btn comment-btn">
        </form>

        <div class="comments">
            <?php foreach($comments as $comment): ?>
                <div class="comment">
                    <p class="content"><?php echo $comment->content; ?></p>
                    <div class="infos">
                        By USER_<?php echo $comment->author_id; ?> on <?php echo $comment->post_date; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php include 'inc/footer.php'; ?>