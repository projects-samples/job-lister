<?php

    include 'inc/header.php';

?>
    <div class="jumbotron">
        <h1>Find a Job</h1>
        <form method="GET" action="index.php">
            <select name="category">
                <option value="0">Choose a category</option>
                <?php foreach($categories as $category): ?>
                    <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                <?php endforeach; ?>
            </select>
            <input type="submit" value="FIND">
        </form>
 
    </div>
    <h3><?php echo $title; ?></h3>
    <div class="job-items marketing">
    <?php foreach($jobs as $job): ?>
        <div class="job-item">
            <div class="job-text">
                <h4><?php echo $job->job_title; ?></h4>
                <p><?php echo $job->description; ?></p>
            </div>
            <div class="job-controls">
                <a href="job.php?id=<?php echo $job->id; ?>" class="view-btn">View</a>
            </div>
        </div>
    <?php endforeach; ?>
    </div>
    
<?php
    include 'inc/footer.php';
?>

