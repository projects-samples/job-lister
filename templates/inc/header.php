<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Job Lister</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<div class="container">
        <div class="header">
            <nav>
                <ul class="nav">
                    <li id="brand_name"><a href="index.php"><?php echo SITE_TITLE; ?></a></li>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="create.php">Create Listing</a></li>
                </ul>
            </nav>
        </div>
        <?php
            displayMessage();
        ?>