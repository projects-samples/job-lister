<?php include 'inc/header.php'; ?>

    <h2 class="page-header">Edit Job Listing</h2>

    <form action="edit.php?id=<?php echo $job->id; ?>" method="post" id="create-job">
        <div class="form-group">
            <label for="company">Company</label>
            <input type="text" class="form-control" id="company" name="company"  value="<?php echo $job->company; ?>">
        </div>
        <div class="form-group">
            <label for="category">Category</label>
            <select class="form-control" id="category" name="category">
                <option value="0">Choose a category</option>
                <?php foreach($categories as $category): ?>
                    <?php if ($category->id == $job->category_id) {?>
                        <option value="<?php echo $category->id; ?>" selected><?php echo $category->name; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                    <?php } ?>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="job-title">Job Title</label>
            <input type="text" class="form-control" id="job-title" name="job_title" value="<?php echo $job->job_title; ?>">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description"><?php echo $job->description; ?></textarea>
        </div>
        <div class="form-group">
            <label for="location">Location</label>
            <input type="text" class="form-control" id="location" name="location" value="<?php echo $job->location; ?>">
        </div>
        <div class="form-group">
            <label for="salary">Salary</label>
            <input type="text" class="form-control" id="salary" name="salary" value="<?php echo $job->salary; ?>">
        </div>
        <div class="form-group">
            <label for="contact-user">Contact User</label>
            <input type="text" class="form-control" id="contact-user" name="contact_user" value="<?php echo $job->contact_user; ?>">
        </div>
        <div class="form-group">
            <label for="contact-email">Contact Email</label>
            <input type="text" class="form-control" id="contact-email" name="contact_email" value="<?php echo $job->contact_email; ?>">
        </div>
        <div class="form-group">
            <input type="submit" value="Update" name="submit">
        </div>
    </form>

<?php include 'inc/footer.php'; ?>
