document.addEventListener('DOMContentLoaded', function() {
        fade_session_message();
});

// fade out gracefully the session message
function fade_session_message() {
    if (null !== document.querySelector('.session-message')) {
        sessionMessage = document.querySelector('.session-message');
        sessionMessage.style.transform = "translateX(100%)";
        setTimeout( () => {
            sessionMessage.style.transform = "translateX(-100%)";
        }, 5000);
    }
}