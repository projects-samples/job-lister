<?php

    // redirection function
    function redirect($page = FALSE, $message = NULL, $message_type = NULL)
    {
        $location = is_string($page) ? $page : $_SERVER['SCRIPT_NAME'];

        if ($message != null)
        {
            $_SESSION['message'] = $message;
        }

        if ($message_type != NULL)
        {
            $_SESSION['message_type'] = $message_type;
        }

        header('Location: '.$location);
        exit;
    }

    // display message
    function displayMessage()
    {
        if (!empty($_SESSION['message']))
        {
            $message = $_SESSION['message'];

            if (!empty($_SESSION['message_type']))
            {
                $message_type = $_SESSION['message_type'];

                if ($message_type == 'error')
                {
                    echo "<div class='session-message alert alert-danger'>".$message."</div>";
                } else
                {
                    echo "<div class='session-message alert alert-success'>".$message."</div>";
                }
            }
            // unset session variables
            unset($_SESSION['message']);
            unset($_SESSION['message_type']);
        } else
        {
            echo '';
        }
    }

    
?>