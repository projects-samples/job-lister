<?php

/**
 * @package Comment
 * @author El-nirina Dama
 */
 class Comment
 {
     /**
      * handler to the database
      * @var object
      */
     private $db;

     public function __construct()
     {
         $this->db = new Database;
     }

     public function getByJobId($job_id)
     {
         $this->db->query("SELECT * FROM comments WHERE post_id=$job_id ORDER BY id DESC");
         return $this->db->resultSet();
     }

     public function create($content, $author_id, $job_id)
     {
         $this->db->query('INSERT INTO comments(content, author_id, post_id)
                VALUES (:content, :author_id,
                :post_id)
         ');
         $this->db->bind(':content', $content);
         $this->db->bind(':author_id', $author_id);
         $this->db->bind(':post_id', $job_id);

         return $this->db->execute() ? true : false;

     }
 }
?>