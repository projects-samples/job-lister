<?php

/**
 * @package Job
 * A class describing the Job Object used as an entity class to interact to the corresponding job table in the database
 * @author El-nirina Dama
 */
class Job
{
    /**
     * @var object
     * represents the connection pool to the DB
     */
    private $db;

    /**
     * The class constructor
     * @return object
     */
    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Get all jobs
     * @return array
     */
    public function getAll()
    {
        $this->db->query("select jobs.*, categories.name as cname
                            from jobs 
                            inner join categories
                            on jobs.category_id=categories.id 
                            order by post_date desc
                        ");
        // assign db result set
        $results = $this->db->resultSet();

        return $results;
    }

    /**
     * Get all categories of jobs
     * @return array
     */
    public function getCategories()
    {
        $this->db->query("select categories.* from categories");
        // assign db result set
        $results = $this->db->resultSet();

        return $results;
    }

    /**
     * Get all jobs related to a category
     * @param int
     * @return array
     */
    public function getByCategory($category)
    {
        $this->db->query("select jobs.*, categories.name as cname
                            from jobs 
                            inner join categories
                            on jobs.category_id=categories.id 
                            where jobs.category_id = $category
                            order by post_date desc
                        ");
        // assign db result set
        $results = $this->db->resultSet();

        return $results;
    }

    /**
     * Get all category infos based on its id
     * @param int
     * @return Object
     */
    public function getCategory($category_id)
    {
        $this->db->query("select categories.* from categories where id = :category_id");

        $this->db->bind(':category_id', $category_id);
        // assign db result set
        $row = $this->db->single();

        return $row;
    }

    /**
     * Get all job's info based on its id
     * @param int
     * @return Object
     */
    public function getJob($id)
    {
        $this->db->query("select * from jobs where id = :id");

        $this->db->bind(':id', $id);
        // assign db result set
        $row = $this->db->single();

        return $row;
    }

    /**
     * Persist a new job in the db
     * @param array
     * @return boolean
     */
    public function create($data)
    {
        $this->db->query("insert into jobs (category_id, job_title, company,
                            description, location, salary, contact_user, 
                            contact_email) values (:category_id, :job_title, 
                            :company, :description, :location, :salary, :contact_user,
                            :contact_email)");
        // bind $data
        $this->db->bind(':category_id', $data['category_id']);
        $this->db->bind(':job_title', $data['job_title']);
        $this->db->bind(':company', $data['company']);
        $this->db->bind(':description', $data['description']);
        $this->db->bind(':location', $data['location']);
        $this->db->bind(':salary', $data['salary']);
        $this->db->bind(':contact_user', $data['contact_user']);
        $this->db->bind(':contact_email', $data['contact_email']);

        return $this->db->execute() ? true : false;
    }

    /**
     * Remove a job from db
     * @param int
     * @return boolean
     */
    public function delete($id)
    {
        $this->db->query("DELETE FROM jobs WHERE id=$id");

        return $this->db->execute() ? true : false;
    }

    /**
     * Update a job
     * @param int
     * @param array
     * @return bool
     */
    public function update($id, $data)
    {
        $this->db->query("UPDATE jobs
            SET
                category_id = :category_id,
                job_title = :job_title,
                company = :company, 
                description = :description,
                location = :location, 
                salary = :salary,
                contact_email = :contact_email,
                contact_user = :contact_user 
            WHERE id = $id");

        // $this->db->query("UPDATE jobs SET description = :description WHERE id = $id");
        // bind $data
        $this->db->bind(':category_id', $data['category_id']);
        $this->db->bind(':job_title', $data['job_title']);
        $this->db->bind(':company', $data['company']);
        $this->db->bind(':description', $data['description']);
        $this->db->bind(':location', $data['location']);
        $this->db->bind(':salary', $data['salary']);
        $this->db->bind(':contact_user', $data['contact_user']);
        $this->db->bind(':contact_email', $data['contact_email']);

        return $this->db->execute() ? true : false;
    }
}   

?>