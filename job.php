<?php

include_once 'config/init.php';

$job = new Job;
$comment = new Comment;

if (isset($_POST['del_id']))
{
    $del_id = $_POST['del_id'];
    if ($job->delete($del_id))
    {
        redirect('index.php', 'Job deleted.', 'success');
    } else
    {
        redirect('index.php', 'Could not delete the job', 'error');
    }
}

if (isset($_POST['comment']))
{
    $post_id = $_POST['post_id'];
    $content = $_POST['content'];
    $author_id = 42;

    if ($content == "")
    {
        $redirect_ui = 'job.php?id='. $post_id;
        redirect($redirect_ui, 'You need to enter a valid comment', 'error');
    }
    
    if ($comment->create($content, $author_id, $post_id))
    {
        $redirect_ui = 'job.php?id='. $post_id;
        redirect($redirect_ui, 'Your comment has been added', 'success');
    } else
    {
        redirect($redirect_ui, 'Something went wrong', 'error');
    }
    
}

$template = new Template("templates/job-single.php");
$job_id = isset($_GET['id']) ? $_GET['id'] : null;

$template->job = $job->getJob($job_id);
$template->comments = $comment->getByJobId($job_id);

echo $template;

?>